ssh_key
=========

Generates new Keys or create existing keys.

Requirements
------------

None

Role Variables
--------------

| Variable                   | Required | Description                                 | Default |
|----------------------------|----------|---------------------------------------------|---------|
| ssh_key__ssh_keys          | true     | List of ssh key parameters and/or ssh keys. | []      |

Dependencies
------------

```yaml
roles:
  - src: https://gitlab.com/ibox-project/roles/ibox.configuration.git
    version: stable
    scm: git
    name: ibox.configuration

collections:
  - name: community.crypto
    version: 2.16.0
```

Example Playbook
----------------
Include the role in `requirements.yml` with something like
```yaml
  - src: https://gitlab.com/ibox-project/roles/ibox.ssh_key.git
    version: stable
    scm: git
    name: ibox.ssh_key
```

and write in the playbook something like

```yaml
  tasks:
    - name: "Include ibox.ssh_key"
      ansible.builtin.include_role:
        name: "ibox.ssh_key"
      vars:
        ssh_key__ssh_keys: 
          - type: rsa
            comment: "key with default parameters"
```

License
-------

Licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
